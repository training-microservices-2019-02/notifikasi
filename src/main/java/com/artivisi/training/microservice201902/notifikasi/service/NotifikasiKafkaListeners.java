package com.artivisi.training.microservice201902.notifikasi.service;

import com.artivisi.training.microservice201902.notifikasi.dto.NotificationRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class NotifikasiKafkaListeners {

    private static final Logger LOGGER = LoggerFactory.getLogger(NotifikasiKafkaListeners.class);

    @Autowired private ObjectMapper objectMapper;

    @KafkaListener(topics = "${kafka.topic.notifikasi.request}")
    public void terimaRequestNotifikasi(String message){
        LOGGER.debug("Terima message request notifikasi [{}]", message);

        try {
            NotificationRequest req = objectMapper.readValue(message, NotificationRequest.class);
            LOGGER.debug("Konversi json menjadi object : [{}]", req);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

    }
}
